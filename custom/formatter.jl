__precompile__()

macro orgOut(NewType::String, func)
    expr = quote
        if isdefined(Symbol($NewType)) && isa(eval(Symbol($NewType)),DataType)
            function org(filename::String, data::$(Symbol(NewType)))
                $(func)(filename, data)
            end
        end
    end
    esc(expr)
end

macro orgArray(NewType::String, func, dims = 0)
    expr = quote
        if isdefined(Symbol($NewType)) && isa(eval(Symbol($NewType)),DataType)
            if $dims == 0 # any
                function org(filename::String, data::Array{$(Symbol(NewType))})
                    $(func)(filename, data)
                end
            else
                function org(filename::String, data::Array{T,$dims} where T<:$(Symbol(NewType)))
                    $(func)(filename, data)
                end
            end
        end
    end
    esc(expr)
end

@orgOut "String"  write
# The following will not trigger an error even if undef is not a valid type.
# We should have it working when a library supporting it is imported
@orgOut "Undef"   write # does not work since undef is undefined
@orgOut "Void"    (x,y) -> org(x, "")
@orgOut "Int"     (x,y) -> org(x,string(y))
@orgOut "Float64" (x,y) -> org(x,string(y))

@orgArray "Float64"    (x,y) -> write(x,string(y))  1
@orgArray "Int"        (x,y) -> write(x,string(y))  1
@orgArray "Float64"    writecsv                       2
@orgArray "Int"        writecsv                       2

# Stringify everything else
org(filename::String, data) = org(filename,string(data))
