;; quick style
(menu-bar-mode -1)
(column-number-mode t)

(defun nx/command-exists (exe)
  (if (executable-find exe)
      t
    nil))

(setq is-nix-p (nx/command-exists "nixos-rebuild"))
(setq is-win-p (if (string= system-type "windows-nt") 't 'nil))

(when is-win-p
  (message "bleah, you are running windows. Sorry guy")
  (require 'package)
  (setq package-archives '(("gnu" . "https://elpa.gnu.org/packages/")
			   ("melpa" . "https://melpa.org/packages/")))
  (package-initialize)
  (if (not (package-installed-p 'use-package))
      (progn (package-refresh-contents)
	     (package-install 'use-package)))

  (require 'use-package)
  (setq use-package-always-ensure t))

(if is-nix-p
    (progn
      (message "nix detected")
      ;; remove default nix from load path to prevent nix-mode from
      ;; nixos overriding melpa nix-mode
      (require 'seq)
      (setq load-path
	    (seq-difference load-path (mapcar (lambda (x) (concat x "/share/emacs/site-lisp/"))
					(split-string (or (getenv "NIX_PROFILES") "")))))

      (require 'package)
      (setq package-archives nil)
      ;; (load "~/.emacs.d/nix-pack.el")
      (package-initialize)
      (setq package-enable-at-startup nil)
      (unless (package-installed-p 'use-package)
        (package-refresh-contents)
        (package-install 'use-package))
      ;; (eval-when-compile (require 'use-package))
      (require 'use-package)
      (require 'bind-key) ;; if you use any :bind variant
      )
  ;; else
  (when (not is-win-p)
    (progn ;; https://github.com/raxod502/straight.el#integration-with-use-package
(let ((bootstrap-file (concat user-emacs-directory "straight/repos/straight.el/bootstrap.el"))
      (bootstrap-version 3))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))
      (setq straight-use-package-by-default t)

      (straight-use-package 'use-package)
)))

;; add those not in m/elpa here
(add-to-list 'load-path "~/.emacs.d/custom")
(add-to-list 'load-path "~/.emacs.d/nx")

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector
   [default default default italic underline success warning error])
 '(column-number-mode t)
 '(compilation-message-face (quote default))
 '(cua-global-mark-cursor-color "#2aa198")
 '(cua-normal-cursor-color "#839496")
 '(cua-overwrite-cursor-color "#b58900")
 '(cua-read-only-cursor-color "#859900")
 '(custom-safe-themes
   (quote
    ("9a155066ec746201156bb39f7518c1828a73d67742e11271e4f24b7b178c4710" "8db4b03b9ae654d4a57804286eb3e332725c84d7cdab38463cb6b97d5762ad26" "d677ef584c6dfc0697901a44b885cc18e206f05114c8a3b7fde674fce6180879" "8aebf25556399b58091e533e455dd50a6a9cba958cc4ebb0aab175863c25b9a4" default)))
 '(elide-head-headers-to-hide
   (quote
    (("is free software[:;] you can redistribute it" . "\\(Boston, MA 0211\\(1-1307\\|0-1301\\), USA\\|If not, see <http://www\\.gnu\\.org/licenses/>\\)\\.")
     ("The Regents of the University of California\\.  All rights reserved\\." . "SUCH DAMAGE\\.")
     ("Permission is hereby granted, free of charge" . "authorization from the X Consortium\\.")
     ("Software License Agreement (BSD License)" . "POSSIBILITY OF SUCH DAMAGE."))))
 '(fci-rule-color "#073642")
 '(highlight-changes-colors (quote ("#d33682" "#6c71c4")))
 '(highlight-symbol-colors
   (--map
    (solarized-color-blend it "#002b36" 0.25)
    (quote
     ("#b58900" "#2aa198" "#dc322f" "#6c71c4" "#859900" "#cb4b16" "#268bd2"))))
 '(highlight-symbol-foreground-color "#93a1a1")
 '(highlight-tail-colors
   (quote
    (("#073642" . 0)
     ("#546E00" . 20)
     ("#00736F" . 30)
     ("#00629D" . 50)
     ("#7B6000" . 60)
     ("#8B2C02" . 70)
     ("#93115C" . 85)
     ("#073642" . 100))))
 '(hl-bg-colors
   (quote
    ("#7B6000" "#8B2C02" "#990A1B" "#93115C" "#3F4D91" "#00629D" "#00736F" "#546E00")))
 '(hl-fg-colors
   (quote
    ("#002b36" "#002b36" "#002b36" "#002b36" "#002b36" "#002b36" "#002b36" "#002b36")))
 '(hl-sexp-background-color "#efebe9")
 '(magit-diff-use-overlays nil)
 '(notmuch-saved-searches
   (quote
    ((:name "inbox" :query "tag:inbox" :key "i")
     (:name "unread" :query "tag:unread" :key "u")
     (:name "flagged" :query "tag:flagged" :key "f")
     (:name "sent" :query "tag:sent" :key "t")
     (:name "drafts" :query "tag:draft" :key "d")
     (:name "all mail" :query "*" :key "a")
     (:name "almost-spam" :query "from:MediaWorld OR from:evensi OR from:\"UCI Cinemas\" OR from:\"Pocket Hits\" OR from:ResearchGate OR from:CARISMI OR from:\"Hydra Build Daemon\" OR from:Once OR from:LinkedIn OR from:YouTube"))))
 '(nrepl-message-colors
   (quote
    ("#dc322f" "#cb4b16" "#b58900" "#546E00" "#B4C342" "#00629D" "#2aa198" "#d33682" "#6c71c4")))
 '(org-agenda-files
   (quote
    ("~/org/inbox.org" "~/PhD/papers/ICMI2017/index.org" "~/org/gtd.org" "~/org/tickler.org")))
 '(org-emphasis-alist
   (quote
    (("*" bold)
     ("/" italic)
     ("_" underline)
     ("=" org-verbatim verbatim)
     ("~" org-code verbatim)
     ("+"
      (:strike-through t))
     ("%" nx/org-highlight))))
 '(org-latex-default-packages-alist
   (quote
    (("AUTO" "inputenc" t
      ("pdflatex"))
     ("T1" "fontenc" t
      ("pdflatex"))
     ("" "graphicx" t nil)
     ("" "grffile" t nil)
     ("" "longtable" nil nil)
     ("" "wrapfig" nil nil)
     ("" "rotating" nil nil)
     ("normalem" "ulem" t nil)
     ("" "amsmath" t nil)
     ("full" "textcomp" t nil)
     ("" "amssymb" t nil)
     ("" "capt-of" nil nil)
     ("" "hyperref" nil nil))))
 '(org-src-window-setup (quote current-window))
 '(org-use-speed-commands t)
 '(package-selected-packages
   (quote
    (tex yarn-mode yaml-mode xkcd writeroom-mode writegood-mode wordnut which-key wgrep web-mode wc-mode vimish-fold use-package undo-tree twittering-mode synosaurus spray solarized-theme smooth-scrolling smartparens smart-tabs-mode smart-comment slirm skewer-mode scratch react-snippets rainbow-mode rainbow-identifiers powerline polymode php-mode perspective pcap-mode ox-reveal ox-pandoc org-ref org-plus-contrib org-pdfview org-page org-mind-map org-download org-caldav org-bullets ob-ipython notmuch-labeler nix-sandbox nix-mode multiple-cursors meson-mode markdown-mode magit magic-latex-buffer lua-mode lorem-ipsum load-relative linum-relative leuven-theme langtool kodi-remote julia-shell jsx-mode jss json-mode ivy-hydra ivy-bibtex interleave impatient-mode iasm-mode hungry-delete highlight-thing haskell-mode gscholar-bibtex gradle-mode gnuplot-mode git-wip-timemachine git-timemachine git-gutter ggtags fontawesome flymake-google-cpplint flycheck-julia flycheck-irony fiplr fic-mode faust-mode expand-region ess emms emmet-mode ember-mode elfeed-org elf-mode dumb-jump dot-mode dashboard csv-mode csharp-mode crux cpputils-cmake counsel-projectile company-nixos-options company-irony company-emacs-eclim company-auctex column-enforce-mode cmake-ide cloc cargo bool-flip benchmark-init bbdb auto-yasnippet auctex-latexmk aggressive-indent ace-window Arduino-mode)))
 '(pos-tip-background-color "#073642")
 '(pos-tip-foreground-color "#93a1a1")
 '(safe-local-variable-values
   (quote
    ((eval setq org-html-checkbox-type
	   (quote html))
     (eval setq org-ref-ref-html "<a href=\"#/bibliography\">%s</a>")
     (eval setq org-ref-bib-html "<a id=\"bibliograpy\"></a>"))))
 '(send-mail-function (quote mailclient-send-it))
 '(smartrep-mode-line-active-bg (solarized-color-blend "#859900" "#073642" 0.2))
 '(term-default-bg-color "#002b36")
 '(term-default-fg-color "#839496")
 '(vc-annotate-background nil)
 '(vc-annotate-background-mode nil)
 '(vc-annotate-color-map
   (quote
    ((20 . "#dc322f")
     (40 . "#c85d17")
     (60 . "#be730b")
     (80 . "#b58900")
     (100 . "#a58e00")
     (120 . "#9d9100")
     (140 . "#959300")
     (160 . "#8d9600")
     (180 . "#859900")
     (200 . "#669b32")
     (220 . "#579d4c")
     (240 . "#489e65")
     (260 . "#399f7e")
     (280 . "#2aa198")
     (300 . "#2898af")
     (320 . "#2793ba")
     (340 . "#268fc6")
     (360 . "#268bd2"))))
 '(vc-annotate-very-old-color nil)
 '(weechat-color-list
   (quote
    (unspecified "#002b36" "#073642" "#990A1B" "#dc322f" "#546E00" "#859900" "#7B6000" "#b58900" "#00629D" "#268bd2" "#93115C" "#d33682" "#00736F" "#2aa198" "#839496" "#657b83")))
 '(xterm-color-names
   ["#073642" "#dc322f" "#859900" "#b58900" "#268bd2" "#d33682" "#2aa198" "#eee8d5"])
 '(xterm-color-names-bright
   ["#002b36" "#cb4b16" "#586e75" "#657b83" "#839496" "#6c71c4" "#93a1a1" "#fdf6e3"]))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

(load "~/.emacs.d/init.el")
(put 'narrow-to-region 'disabled nil)
