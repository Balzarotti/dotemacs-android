(setq debug 'nil)
(setq debug-on-error debug)

(condition-case t
    (progn
      (when debug
	(use-package benchmark-init)
	(benchmark-init/activate))

      (setq gui-enabled (or is-nix-p is-win-p))
      ;; Load the theme ASAP to reduce flickering
      (require 'nx-apparence)

      (require 'nx-emacs-server)

      (require 'nx-keymap)

      (require 'nx-git)
      (require 'nx-history)

      (require 'nx-ivy)
      (require 'nx-company)
      (require 'nx-behaviour)

      ;; helpers config:
      (setq nx/enable-abbrev-autoadd t)
      (require 'nx-helpers)

      (require 'nx-languages)
      (require 'nx-nix)

      (require 'nx-calendar)
      (require 'nx-org)

      (require 'nx-extra)))

(if debug (benchmark-init/deactivate))
