;; Abbrev-spellcheck
(defun endless/simple-get-word ()
  (car-safe (save-excursion (ispell-get-word nil))))
;; press "i" to add to dict, "a" to add session-local
;; and "A" to add buffer-local
(defun endless/ispell-word-then-abbrev (p)
  "Call `ispell-word', then create an abbrev for it.
With prefix P, create local abbrev. Otherwise it will
be global.
If there's nothing wrong with the word at point, keep
looking for a typo until the beginning of buffer. You can
skip typos you don't want to fix with `SPC', and you can
abort completely with `C-g'."
  (interactive "P")
  (let (bef aft)
    (save-excursion
      (while (if (setq bef (endless/simple-get-word))
		 ;; Word was corrected or used quit.
		 (if (ispell-word nil 'quiet)
		     nil ; End the loop.
		   ;; Also end if we reach `bob'.
		   (not (bobp)))
	       ;; If there's no word at point, keep looking
	       ;; until `bob'.
	       (not (bobp)))
	(backward-word)
	(backward-char))
      (setq aft (endless/simple-get-word)))
    (when nx/enable-abbrev-autoadd
      (if (and aft bef (not (equal aft bef)))
	  (let ((aft (downcase aft))
		(bef (downcase bef)))
	    (define-abbrev
	      (if p global-abbrev-table local-abbrev-table)
	      bef aft)
	    (message "\"%s\" now expands to \"%s\" %sally"
		     bef aft (if p "loc" "glob")))
	(user-error "No typo at or before point")))))

(defun spacemacs/kill-this-buffer ()
  "Kill the current buffer."
  (interactive)
  (if (window-minibuffer-p)
      (abort-recursive-edit)
    (kill-buffer (current-buffer))))


(defun nx/switch-to-buffer-or-window (buffer)
  (let ((window-frame (get-buffer-window buffer 'visible)))
    (if window-frame
        (select-window window-frame)
      (progn
        (ivy--switch-buffer-action buffer)))))

(defun nx/ivy-switch-buffer ()
  "Switch to another buffer."
  (interactive)
  (let ((this-command 'nx/switch-to-buffer-or-window))
    (ivy-read "Switch to buffer: " 'internal-complete-buffer
              :matcher #'ivy--switch-buffer-matcher
              :preselect (buffer-name (other-buffer (current-buffer)))
              :action #'nx/switch-to-buffer-or-window
              :keymap ivy-switch-buffer-map
              :caller 'ivy-switch-buffer)))

(defun narrow-to-region-indirect (start end)
  "Restrict editing in this buffer to the current region, indirectly."
  (interactive "r")
  (deactivate-mark)
  (let ((buf (clone-indirect-buffer nil nil)))
    (with-current-buffer buf
      (narrow-to-region start end))
    (switch-to-buffer buf)))

(defun narrow-or-widen-dwim (p)
  "Widen if buffer is narrowed, narrow-dwim otherwise.
Dwim means: region, org-src-block, org-subtree, or
defun, whichever applies first. Narrowing to
org-src-block actually calls `org-edit-src-code'.

With prefix P, don't widen, just narrow even if buffer
is already narrowed."
  (interactive "P")
  (declare (interactive-only))
  (cond ((and (buffer-narrowed-p) (not p)) (widen))
        ((region-active-p)
         (narrow-to-region (region-beginning)
                           (region-end)))
        ((derived-mode-p 'org-mode)
         ;; `org-edit-src-code' is not a real narrowing
         ;; command. Remove this first conditional if
         ;; you don't want it.
         (cond ((ignore-errors (org-edit-src-code) t)
                ;; (delete-other-windows) ; I want to keep the other window open
		;; also remember to set (setq org-src-window-setup 'current-window)
		)
               ((ignore-errors (org-narrow-to-block) t))
               (t (org-narrow-to-subtree))))
        ((derived-mode-p 'latex-mode)
         (LaTeX-narrow-to-environment))
        (t (narrow-to-defun))))

;; Required sometimes
(defun nx/clear-kill-ring ()
  (interactive)
  (setq kill-ring nil) (garbage-collect))

;; Function to encode/decode url strings
(defun func-region (start end func)
  "run a function over the region between START and END in current buffer."
  (save-excursion
    (let ((text (delete-and-extract-region start end)))
      (insert (funcall func text)))))

(defun url-encode (start end)
  "urlencode the region between START and END in current buffer."
  (interactive "r")
  (func-region start end #'url-hexify-string))

(defun url-decode (start end)
  "de-urlencode the region between START and END in current buffer."
  (interactive "r")
  (func-region start end #'url-unhex-string))

(defun narrow-to-region-indirect (start end)
  "Restrict editing in this buffer to the current region, indirectly."
  (interactive "r")
  (deactivate-mark)
  (let ((buf (clone-indirect-buffer nil nil)))
    (with-current-buffer buf
      (narrow-to-region start end))
    (switch-to-buffer buf)))

;; FIXME
(defun find-project-json ()
  (interactive "P")
  "Look for the first shell.nix file in the directories
   above the current file, search for a buildClang directory,
   and set irony json to this one."
  (irony-cdb-json--load-db
   (concat (concat
            (concat (locate-dominating-file default-directory "shell.nix")
                    "buildClang") "/")
           "compile_commands.json")))

;; FIXME: never used?
(defun complete-or-indent ()
  (interactive)
  (if (company-manual-begin)
      (company-complete-common)
    (indent-according-to-mode)))

(require 'nx-org-insert-macro)
;; (require 'nx-org-format)
(require 'nx-org-capture-notmuch)

(provide 'nx-functions)
