(use-package notmuch :after (org pdf-tools) :defer t
  :init
  (setq notmuch-archive-tags '("-inbox")
        notmuch-show-logo nil
        notmuch-search-oldest-first nil
        notmuch-hello-thousands-separator ".")
  :config
  (add-hook 'notmuch-message-mode-hook #'orgstruct++-mode)
  (add-hook 'notmuch-message-mode-hook #'flyspell-mode)
  :commands (notmuch)
  :bind (:map notmuch-search-mode-map
	      ("g" . notmuch-poll-and-refresh-this-buffer)
	      :map notmuch-show-mode-map
	      ("c" . nx/capture-this-mail)
	      ("o" . notmuch-show-view-part)
	      ("O" . org-open-at-point))
  );; notmuch:<terms>, notmuch-search:<terms>
;;(use-package org-caldav :after org)

(use-package notmuch-labeler :after notmuch :defer t
  :disabled
  :config
  (notmuch-labeler-rename "unread" "new" ':foreground "blue")
  (notmuch-labeler-rename "inbox" "inbox" ':foreground "green")
  (notmuch-labeler-rename "flagged" "starred" ':foreground "yellow")
  ;; (notmuch-labeler-image-tag "flagged")
  (notmuch-labeler-hide "unread"))

(setq mail-user-agent 'message-user-agent)
(setq user-mail-address "anothersms@gmail.com"
      user-full-name "Nicolò Balzarotti")
(setq smtpmail-stream-type 'ssl
      smtpmail-smtp-server "smtp.gmail.com"
      smtpmail-smtp-service 465)
;; https://kkatsuyuki.github.io/notmuch-conf/
(setq send-mail-function 'smtpmail-send-it
      message-send-mail-function 'message-smtpmail-send-it)
(setq message-default-mail-headers "Cc: \nBcc: \n")

(use-package elfeed-org :after org :defer t :if is-nix-p
  :init (setq rmh-elfeed-org-files (list "~/.emacs.d/elfeed.org")))

(use-package elfeed :requires elfeed-org :after org :defer t
  :init (progn
	  (add-hook 'elfeed-new-entry-hook
	  	    (elfeed-make-tagger :entry-title "Friday Squid Blogging.*"
					:add 'junk
					:remove 'unread))
	  (elfeed-org)
	  ;; automatically export the feed list to opml
	  ;; that way it can be imported on android etc
	  (elfeed-export-opml "~/.emacs.d/elfeed.opml"))
  :bind (:map elfeed-search-mode-map
              ("m" . elfeed-search-show-entry)))

(use-package twittering-mode :defer t :commands (twit)
  :init (setq twittering-icon-mode t;; Display icons
	      twittering-use-icon-storage t;; Save icons
	      twittering-use-master-password t;; Save password
	      ))
;; Dired
;; emacs rocks #16
(setq dired-dwim-target t) ; move files between split panes
;; ;; (use-package dired-details
;; ;;   :init
;; ;;   (setq-default dired-details-hidden-string "⊢ ")
;; ;;   :config (dired-details-install))

(use-package bbdb
  :if (display-graphic-p)
  :defer t :commands (bbdb)
  :config
  (bbdb-initialize 'gnus 'mail 'message 'anniv)

  (setq bbdb-complete-mail-allow-cycling t
        bbdb-allow-duplicates t
        bbdb-message-all-addresses t
        bbdb-file
        (expand-file-name "contacts.bbdb.gz" "~/.emacs.d"))

  (add-hook 'message-setup-hook 'bbdb-mail-aliases)

  (defun org-bbdb-export (path desc format)
    "Create the export version of a BBDB link specified by PATH or DESC.
If exporting to either HTML or LaTeX FORMAT the link will be
italicized, in all other cases it is left unchanged."
    (setq desc path)
    (cond
     ((eq format 'html) (format "<i>%s</i>" desc))
     ((eq format 'latex) (format "\\textit{%s}" desc))
     ((eq format 'odt)
      (format "<text:span text:style-name=\"Emphasis\">%s</text:span>" desc))
     (t desc))))

(provide 'nx-organizer)
