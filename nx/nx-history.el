(progn ; save files settings
  (shell-command "mkdir -p ~/.saves") ;; FIXME: implement like modi/undo-tree
  (setq backup-directory-alist '(("." . "~/.saves"))
	;; FIXME: Change backup folder
	backup-by-copying t ;; Copy: slower, safer
	delete-old-versions t ;; Delete without asking
	kept-new-versions 6
	kept-old-versions 2
	version-control t))

(use-package undo-tree ; can't defer since we need global-undo-tree-mode
  :init
  (setq undo-tree-auto-save-history t
	undo-tree-visualizer-diff t
	;; Persistent undo-tree history across emacs sessions
	modi/undo-tree-history-dir (let ((dir (concat user-emacs-directory
						      "undo-tree-history/")))
				     (make-directory dir :parents)
				     dir)
	undo-tree-history-directory-alist `(("." . ,modi/undo-tree-history-dir)))
  :config
  (add-hook 'write-file-functions #'undo-tree-save-history-hook)
  (add-hook 'find-file-hook #'undo-tree-load-history-hook)
  ;; Compress saved history
  (defadvice undo-tree-make-history-save-file-name
      (after undo-tree activate)
    (setq ad-return-value (concat ad-return-value ".gz")))
  ;; TODO: use macros
  (defun undo-tree-10-redo () (interactive)
	 (dotimes (x 10) (undo-tree-visualize-redo)))
  (defun undo-tree-10-undo () (interactive)
	 (dotimes (x 10) (undo-tree-visualize-undo)))
  (global-undo-tree-mode 1)
  :bind (("M-\\" . undo-tree-visualize)
	 ;; Mouse! Scroll through time
	 ("M-<mouse-5>" . undo-tree-redo)
	 ("M-<mouse-4>" . undo-tree-undo)
	 :map undo-tree-visualizer-mode-map
	 ("M-\\" . undo-tree-visualizer-quit) ; toggle on-off like quake
	 ("<mouse-5>" . undo-tree-visualize-redo)
	 ("<mouse-4>" . undo-tree-visualize-undo)
	 ;; Page scrolling on history!
	 ("M-v" . undo-tree-10-undo)
	 ("C-v" . undo-tree-10-redo)))

(use-package git-timemachine :after magit :defer t
  :bind
  ;; ("C-x t" . git-timemachine-toggle)
  ("C-M-<mouse-5>" . next-timemachine)
  ("C-M-<mouse-4>" . prev-timemachine)
  :config
  (defun next-timemachine ()
    (interactive)
    (unless (bound-and-true-p git-timemachine-mode)
      (git-timemachine-toggle))
    (git-timemachine-show-next-revision))
  (defun prev-timemachine ()
    (interactive)
    (unless (bound-and-true-p git-timemachine-mode)
      (git-timemachine-toggle))
    (git-timemachine-show-previous-revision)))

(provide 'nx-history)
