(defun nx/inser-match (arg) (insert (concat "{{{" (car arg) "}}}")))
(defun nx/org-insert-macro ()
  "Find marcos defined in the org document, and insert it"
  (interactive)
  (let ((cands (mapcar 'cdr (s-match-strings-all
			     "\#\\+MACRO:[ ]+\\([^\s]+\\)"
			     (buffer-string)))))
    (ivy-read "Macros: " cands
	      :action 'nx/inser-match)))

(provide 'nx-org-insert-macro)
