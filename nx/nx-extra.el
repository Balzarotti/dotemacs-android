(require 'nx-organizer)

(use-package kodi-remote :defer t :commands (kodi)
  :init (defalias 'kodi 'kodi-remote-keyboard
	  "Easier to remember kodi-remote-keyboard name")
  :config
  (setq kodi-host-name "kodi:media@192.168.1.2:80")
  :bind (:map kodi-remote-keyboard-mode-map
              ("C-n" . kodi-remote-input-down)
              ("C-p" . kodi-remote-input-up)
              ("C-b" . kodi-remote-input-left)
              ("C-f" . kodi-remote-input-right)
              ("C-m" . kodi-remote-input-enter)))

;; Requires an mpd server
(setq mpc-host "pass@localhost:6600")
(use-package emms :defer t
  ;; don't know which command to use
  ;; :commands (emms-player-mpd-play)
  :init
  (setq emms-player-mpd-server-name "localhost")
  (setq emms-player-mpd-server-port "6600")
  ;; FIXME: change pass
  (setq emms-player-mpd-server-password "remoteMPD1299"))

(require 'lyric-mode)

;; binaries: readelf, objdump
(use-package elf-mode :defer t
  :mode ("\\.a$" "\\.so$"))
(use-package iasm-mode :defer t) ; objdump
(require 'objdump)
;; (require 'ksyms)

;; pcap binaries - uses tshark
(use-package pcap-mode :defer t :mode "\\.cap$")
;; (use-package hexl-mode
;;   ;; counsel (because of regex) for binary files
;;   :bind (:mode hexl-mode-map ("C-s" . isearch-forward)))

;; web-get
(use-package web :defer t :commands (web-get))

(use-package xkcd :defer t :commands (xkcd))

;; ;; Games
;; (use-package typing :defer t)
;; (use-package vimgolf :defer t)

(use-package spray ; speed reading
  :defer t :commands (spray-mode))
;; Pair programming
;; https://github.com/tjim/lockstep
;; (use-package lockstep)

(when (nx/command-exists "enchive")
  (use-package enchive-mode :defer t
    :commands (enchive-mode)
    :mode "\\.enchive$"))

(use-package sx :defer t
  ;; StackExchange search
  :bind ("C-h x" . sx-search))

;; Emoji support!
(use-package emojify
  :init
  ;; Display emojis as images if supported, else unicode
  ;; Should be called before emojify-mode
  (if (image-type-available-p 'imagemagick)
      (setq emojify-display-style 'image)
    (setq emojify-display-style 'unicode))
  (emojify-mode) ;; enable everywhere
  ;; Like 'insert-char' (C-x 8 RET) but for emojis!
  :bind ("C-x 9 RET" . emojify-insert-emoji)
  :config
  ;; disable plain text emojis (like :D )
  (emojify-set-emoji-styles '(unicode github)))

(provide 'nx-extra)
