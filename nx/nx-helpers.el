(use-package smart-comment
  :bind
  ("<C-tab>" . smart-comment))
(use-package crux
  :bind
  ("C-c e" . crux-eval-and-replace)
  ("C-c o" . crux-open-with)
  ;; ("C-k" . crux-smart-kill-line)
  ("C-c d" . crux-duplicate-current-line-or-region)
  ("C-c M-d" . crux-duplicate-and-comment-current-line-or-region))

(use-package scratch :defer t
  :commands (scratch))

(use-package bool-flip
  :bind ("C-c b" . bool-flip-do-flip)
  :config ;; FIXME: make language specific
  (add-to-list 'bool-flip-alist  '("import" . "using")); julia
  (add-to-list 'bool-flip-alist  '("t" . "nil")); elisp
  )

(use-package dash)

(use-package expand-region
  :bind ("C-@" . er/expand-region))

;; Can't remember else
(defalias 'xml-pretty-print 'sgml-pretty-print)

(require 'nx-functions)

(provide 'nx-helpers)
