(use-package ox-reveal :after org
  :config (setq org-reveal-mathjax t))
(use-package htmlize :after ox-reveal :defer t)
(use-package ox-latex :after org :defer t
  :config
  (add-to-list 'org-latex-classes
               '("beamer"
                 "\\documentclass\[presentation\]\{beamer\}"
                 ("\\section\{%s\}" . "\\section*\{%s\}")
                 ("\\subsection\{%s\}" . "\\subsection*\{%s\}")
                 ("\\subsubsection\{%s\}" . "\\subsubsection*\{%s\}")))
  (add-to-list 'org-latex-classes
               '("acmart"
		 "\\documentclass{acmart}"
		 ("\\section{%s}" . "\\section*{%s}")
		 ("\\subsection{%s}" . "\\subsection*{%s}")
		 ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
		 ("\\paragraph{%s}" . "\\paragraph*{%s}")))
  (add-to-list 'org-latex-classes
               '("article"
		 "\\documentclass{article}
		 \\usepackage{natbib}"
		 ("\\section{%s}" . "\\section*{%s}")
		 ("\\subsection{%s}" . "\\subsection*{%s}")
		 ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
		 ("\\paragraph{%s}" . "\\paragraph*{%s}"))))
;;   (add-to-list 'org-latex-classes
;;                '("book"
;;                  "\\documentclass{book}"
;;                  ("\\part{%s}" . "\\part*{%s}")
;;                  ("\\chapter{%s}" . "\\chapter*{%s}")
;;                  ("\\section{%s}" . "\\section*{%s}")
;;                  ("\\subsection{%s}" . "\\subsection*{%s}")
;;                  ("\\subsubsection{%s}" . "\\subsubsection*{%s}")))
;;   (add-to-list 'org-latex-classes
;;                '("book"
;;                  "\\documentclass{book}"
;;                  ("\\part{%s}" . "\\part*{%s}")
;;                  ("\\chapter{%s}" . "\\chapter*{%s}")
;;                  ("\\section{%s}" . "\\section*{%s}")
;;                  ("\\subsection{%s}" . "\\subsection*{%s}")
;;                  ("\\subsubsection{%s}" . "\\subsubsection*{%s}"))))

;; General - pandoc
(use-package ox-pandoc :defer t :after org
  :config
  ;; default options for all output formats
  (setq org-pandoc-options '((standalone . t))
	;; special settings for beamer-pdf and latex-pdf exporters
	org-pandoc-options-for-beamer-pdf '((pdf-engine . "xelatex"))
	org-pandoc-options-for-latex-pdf '((pdf-engine . "xelatex"))
	org-pandoc-options-for-html-pdf ''(self-contained . 't)))

;; Blogging
(use-package org-page :defer t :after org
  :init
  (setq op/repository-directory "~/blog")
  (setq op/site-domain "https://nixo.keybase.pub/")
;;; the configuration below are optional
  )

(provide 'nx-ox-org)
