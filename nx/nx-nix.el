(use-package nix-sandbox :if (nx/command-exists "nix-env"))
(use-package nix-mode :defer t
  :commands (nix-shell nix-format-buffer)
  :mode "\\.nix$"
  :config
  (when (nx/command-exists "nix-shell") (require 'nix-shell))
  (when (nx/command-exists "nix-repl")
    (require 'nix-repl)))

(use-package nixos-options :defer t
  :commands (company-nixos-options)
  :init (add-hook 'nix-mode-hook
		  (lambda ()
		    (set
		     (make-local-variable 'company-backends)
		     '(company-nixos-options)))))

(use-package nix-buffer)
(provide 'nx-nix)
