(setq visible-bell 'nil);; Stop to beep on windows for everything
(require 'nx-keyboard-encoding)

;; Behaviour
(setq enable-recursive-minibuffers t) ;; TODO: Try and decide
(setq scroll-error-top-bottom t)
(delete-selection-mode)
(global-hl-line-mode t)
(setq vc-follow-symlinks t)
(show-paren-mode t)
(setq show-paren-delay 0)
(setq scroll-preserve-screen-position t)
(setq tab-always-indent 'complete)
;; (setq-default global-visual-line-mode t)

;; enable the set-goal column command
(put 'set-goal-column 'disabled nil)
;; you can call it with C-x C-n. C-n will save the current line we are
;; on when you call set-goal-column and move to it
;; See: http://emacsblog.org/2007/03/17/quick-tip-set-goal-column/

(defun delete-whitespace-on-save ()
  (add-hook 'before-save-hook 'delete-trailing-whitespace))
(defun no-delete-whitespace-on-save ()
  (remove-hook 'before-save-hook 'delete-trailing-whitespace))
(delete-whitespace-on-save)

(setq create-lockfiles nil)
(global-set-key (kbd "C-x k") 'spacemacs/kill-this-buffer)

;; Tabs
(setq-default indent-tabs-mode t)
(use-package smart-tabs-mode
  :defer 3
  :config (smart-tabs-mode t))

(define-key endless/toggle-map "n"
  #'narrow-or-widen-dwim)
(define-key endless/toggle-map "s"
  #'narrow-to-region-indirect)
;; This line actually replaces Emacs' entire narrowing
;; keymap, that's how much I like this command. Only
;; copy it if that's what you want.
(define-key ctl-x-map "n" #'narrow-or-widen-dwim)

;;startup
(setq inhibit-startup-screen t)

;; Disable C-z suspend
(global-unset-key [(control z)])
(global-unset-key [(control x)(control z)])
(global-set-key (kbd "C-đ") 'forward-symbol)
(global-set-key (kbd "C-”") 'sp-backward-symbol)
;; Delete all whitespaces
(use-package hungry-delete :defer 2
  :config
  (global-hungry-delete-mode t))

(use-package multiple-cursors :defer t
  :bind (("M-1" . mc/mark-previous-lines)
	 ("M-2" . mc/mark-next-lines)
	 ("M-3" . mc/mark-next-like-this)
         ("M-4" . mc/mark-previous-like-this)
         ("M-£" . mc/unmark-next-like-this)
         ("M-$" . mc/unmark-previous-like-this)
         ("C-M-<mouse-1>" . mc/add-cursor-on-click)
         ("C-x RET" . mc/mark-all-symbols-like-this)
         :map endless/mc-map
         ("a" . mc/mark-all-like-this)
         ("i" . mc/insert-numbers)
         ("l" . mc/insert-letters)))

(use-package aggressive-indent ; FIXME: load after juia
  :defer t :commands (aggressive-indent-mode)
  :config
  (add-hook 'emacs-lisp-mode-hook #'aggressive-indent-mode)
  (add-hook 'julia-mode-hook #'aggressive-indent-mode)
  (add-hook 'c++-mode-hook #'aggressive-indent-mode))

(use-package avy :defer t
  :bind
  ("M-," . avy-goto-word-or-subword-1)
  ;; just like goto-line (replaced with this keybinding)
  ;; but powered by avy
  ("M-g g" . avy-goto-line)
  ("M-g M-g" . avy-goto-line))

;; (use-package fiplr
;;   :bind ("C-c f" . fiplr-find-file))

(use-package which-key
  :defer 6
					;  :
  :config (which-key-mode))

(use-package ace-window :defer t
  :config (setq aw-scope 'frame)
  :bind ("C-x o" . ace-window))
;; to be tested
;; (use-package bm)

(use-package ggtags :defer t
  :commands (ggtags-mode ggtags-find-tag-dwim)
  :bind ("M-." . ggtags-find-definition)
  ;; Requires ctags
  :init (setenv "GTAGSLABEL" "ctags"))

(use-package dumb-jump :requires ivy
  :defer t
  :bind (("M-g o" . dumb-jump-go-other-window)
         ("M-g j" . dumb-jump-go)
         ("M-g b" . dumb-jump-back)
         ("M-g q" . dumb-jump-quick-look)
         ("M-g x" . dumb-jump-go-prefer-external)
         ("M-g z" . dumb-jump-go-prefer-external-other-window))
  :config
  (setq dumb-jump-selector 'ivy)
  (setq dumb-jump-prefer-searcher 'rg))

;; Save last point you were on a file
(use-package saveplace
  :defer 1
  :config (setq-default save-place t))

;; TODO: Test if is wanted
;; (setq enable-recursive-minibuffers t)

(use-package vimish-fold :after avy
  :defer 2 ;; don't defer to get persistent fold
  :init (vimish-fold-global-mode 1) ;; persistent folding
  :config (require 'vimish-helpers)
  :bind (("C-'" . nx/vimish-fold-or-create)
         ("C-M-<tab>" . vimish-fold-toggle)))

(define-key ctl-x-map "n" #'narrow-or-widen-dwim)

;; ;; replaced with smartparens
;; (defvar my-electric-pair-modes
;;   '(python-mode org-mode julia-mode web-mode angular-mode
;;                 json-mode c++-mode c-mode nix-mode org-mode
;; 		csharp-mode))
;; (defun my-inhibit-electric-pair-mode (char)
;;   (not (member major-mode my-electric-pair-modes)))
;; (setq electric-pair-inhibit-predicate #'my-inhibit-electric-pair-mode
;;       electric-pair-mode t)
;; (electric-pair-mode)

(use-package yasnippet :defer 8 ;; wait a bit before loading
  :config
  (add-to-list 'yas-snippet-dirs "~/.emacs.d/yasnippet-snippets/snippets")
  (yas-global-mode 1)
  ;; use (yas-reload-all) to reload
  )
(use-package auto-yasnippet :requires yasnippet :defer t
  :bind
  ("M-W" . aya-create)
  ("M-Y" . aya-expand))

(use-package projectile
  :disabled
  :config (projectile-mode)
  ;; Last update (20171009.848) made exploring org files
  ;; extremely slow. This is a workaround, keep an eye on
  ;; https://github.com/bbatsov/projectile/issues/1183
  (setq projectile-mode-line
	'(:eval (format " Projectile[%s]"
                        (projectile-project-name)))))
(use-package counsel-projectile :requires (projectile dash)
  :disabled
  :config (counsel-projectile-on))

;; FIXME:
;; (use-package dashboard :requires projectile
;;   :config
;;   (setq dashboard-banner-logo-title "Welcome to Emacs Dashboard")
;;   (setq dashboard-startup-banner 'official)
;;   (setq dashboard-items '((recents  . 5)
;;                           (bookmarks . 5)
;;                           (agenda . 5)
;;                           (projects . 5)
;;                           ))
;;   :init
;;   (require 'nx-functions/dashboard)
;; ;;;; Use this to customize:
;;   ;; (defun dashboard-insert-custom (number)
;;   ;;   (insert "Custom text"))
;;   ;; (add-to-list 'dashboard-item-generators  '(custom . dashboard-insert-custom))
;;   ;; (add-to-list 'dashboard-items '(custom . 5) t)
;;   (dashboard-setup-startup-hook))

(use-package smartparens
  :defer 4 ;; wait a bit before loading
  :config
  (setq sp-show-pair-from-inside nil)
  (require 'smartparens-config)
  (smartparens-global-mode))

(provide 'nx-behaviour)
