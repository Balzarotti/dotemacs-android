;; ivy-counsel-swiper
(use-package counsel :defer t
  :bind
  ("M-x" . counsel-M-x)
  ("C-x C-f" . counsel-find-file)
  ("<f2> u" . counsel-unicode-char)
  ("C-c g" . counsel-git)
  ("C-c j" . counsel-git-grep)
  ("C-c l" . counsel-locate)
  ("C-c k" . counsel-rg)
  ("M-y" . counsel-yank-pop))

(use-package ivy :demand
  :config
  (ivy-mode 1)
  (setq ivy-use-virtual-buffers t
        ivy-count-format "%d/%d "
        ivy-display-style 'fancy)
  ;; ("C-x b" . my/ivy-switch-buffer)
  ;; Never really used, when are they useful?
  ;; :bind
  ;; ("C-c C-r" . ivy-resume)
  ;; ("<f6>" . ivy-resume)
  )

(use-package swiper :defer t
  :config
  (setq recenter-positions '(top middle bottom))
  :bind
  ("C-s" . swiper)
  ("C-l" . recenter-top-bottom))

;; (byte-recompile-directory package-user-dir nil 'force)

(use-package hydra
  :config (require 'nx-hydra))

(use-package wgrep :requires ivy
  :defer t :commands (wgrep-change-to-wgrep-mode))

(provide 'nx-ivy)
