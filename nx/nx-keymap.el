(define-prefix-command 'endless/mc-map)
(define-key ctl-x-map "m" 'endless/mc-map)
(define-prefix-command 'endless/toggle-map)
;; The manual recommends C-c for user keys, but C-x t is
;; always free, whereas C-c t is used by some modes.
(define-key ctl-x-map "t" 'endless/toggle-map)

(provide 'nx-keymap)
