;; Add this to nixos
;; cat > "${HOME}/.local/share/applications/org-protocol.desktop" << EOF
;; [Desktop Entry]
;; Name=org-protocol
;; Exec=emacsclient %u
;; Type=Application
;; Terminal=false
;; Categories=System;
;; MimeType=x-scheme-handler/org-protocol;
;; EOF

;; And then

;; update-desktop-database ~/.local/share/applications/

(require 'org-protocol)

(setq org-capture-templates
      `(("p" "Protocol" entry (file+headline ,(concat org-directory "inbox.org") "Inbox")
	 "* %^{Title}\nSource: %u, %c\n #+BEGIN_QUOTE\n%i\n#+END_QUOTE\n\n\n%?")
	("L" "Protocol Link" entry (file+headline ,(concat org-directory "inbox.org") "Inbox")
	 "* %? [[%:link][%:description]] \nCaptured On: %U")
	))

;; Kill the frame if one was created for the capture
(defvar kk/delete-frame-after-capture 0 "Whether to delete the last frame after the current capture")

(defun kk/delete-frame-if-neccessary (&rest r)
  (cond
   ((= kk/delete-frame-after-capture 0) nil)
   ((> kk/delete-frame-after-capture 1)
    (setq kk/delete-frame-after-capture (- kk/delete-frame-after-capture 1)))
   (t
    (setq kk/delete-frame-after-capture 0)
    (delete-frame))))

(advice-add 'org-capture-finalize :after 'kk/delete-frame-if-neccessary)
(advice-add 'org-capture-kill :after 'kk/delete-frame-if-neccessary)
(advice-add 'org-capture-refile :after 'kk/delete-frame-if-neccessary)

(provide 'org-protocol-cfg)
