(use-package csharp-mode
  :defer t :mode "\\.cs$")

(use-package vala-mode
  :defer t :mode "\\.vala$")

(provide 'nx-programming-c-like)
