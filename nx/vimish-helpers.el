(defun nx/vimish-fold-avy ()
  (interactive)
  (when (not mark-active)
    (mark))
  (let ((end (let ((avy-all-windows 'nil))
	       (call-interactively #'avy-goto-line)
	       (+ 1 (point)))))
    (nx/vimish-fold-delete-region beg end)
    (vimish-fold beg end)))

(defun nx/vimish-fold-in-region-p (beg end)
  (let ((found 'nil))
    (dolist (overlay (overlays-in beg end))
      (when (vimish-fold--vimish-overlay-p overlay)
	(goto-char (overlay-start overlay))
	(setq found 't)))
    found))

(defun nx/vimish-fold-delete-region (beg end)
  (mapc #'vimish-fold--delete
	(vimish-fold--folds-in beg end)))

(defun nx/vimish-fold-or-create (beg end)
  "Use most of vimish-fold with only one key combination!
   mark a region to fold
   mark over other regions to expand the fold region
   point on a foldable region to fold-unfold
   point out of anything to avy-fold from point
   mark current point on an existing region to delete
  "
  (interactive "r")
  (if (use-region-p)
      ;; create new fold or expand existing
      (progn
	(when (nx/vimish-fold-in-region-p beg end)
	  ;; expand by deleting
	  (nx/vimish-fold-delete-region beg end))
	;; and create a new one
	(vimish-fold beg end)))
  ;; toggle or delete or avy
  (if (nx/vimish-fold-in-region-p (point) (+ 1 (point)))
      ;; toggle or delete
      (if mark-active
	  (progn
	    (nx/vimish-fold-delete-region (point) (+ 1 (point)))
	    (deactivate-mark))
	(vimish-fold-toggle))
    ;; avy
    (nx/vimish-fold-avy)))

(defun nx/vimish-fold-region ()
  "Select region at point and vimish-fold it"
  (interactive)
  (er/expand-region 1)
  (let ((beg (min (point) (mark)))
	(end (max (point) (mark))))
    (vimish-fold beg end)
    (deactivate-mark)))

(provide 'vimish-helpers)
