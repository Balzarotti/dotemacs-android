(use-package company :defer 3
  :diminish (company-mode . "CPY")
  :init
  ;; must be added here so that when enable
  (eval-after-load 'company
    '(add-to-list 'company-backends 'company-irony))
  :config
  (setq company-idle-delay             0.1
        company-show-numbers           t
        ;; company-minimum-prefix-length  5
        dabbrev-case-replace           nil
        company-minimum-prefix-length  2
        company-tooltip-limit          20
        company-dabbrev-downcase       nil)
  (global-company-mode 1))

(use-package company-irony :requires irony
  :defer t :commands (irony-cdb-autosetup-compile-options)
  :config (add-hook 'irony-mode-hook 'irony-cdb-autosetup-compile-options))

;; (defun my-irony-mode-hook ()
;;   (define-key irony-mode-map [remap completion-at-point]
;;     'irony-completion-at-point-async)
;;   (define-key irony-mode-map [remap complete-symbol]
;;     'irony-completion-at-point-async))
;; (add-hook 'irony-mode-hook 'my-irony-mode-hook)

(provide 'nx-company)
