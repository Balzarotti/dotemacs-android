;; Not working, needs to fix
;; TODO: expand org-marcos
(defun nx/org-abstract-lenght-notify ()
  (interactive)
  (message "%s" (nx/org-abstract-length)))

(defun nx/org-abstract-lenght-insert-at-point ()
  (interactive)
  (insert (number-to-string (nx/org-abstract-length))))

(defun nx/org-abstract-length
    (let ((str (format "%s" (s-match
			     "^#\\+BEGIN_abstract\\(.\\|\n\\)*#\\+END_abstract$"
			     (buffer-string)))))
      (if (string= str "")
	  t
	(s-count-matches "\\w+" str))))
