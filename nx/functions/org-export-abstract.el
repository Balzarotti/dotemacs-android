(defun nx/export-abstract ()
  "This function exports the abstract of the current org file to an ASCII buffer"
  (interactive)
  (set-mark (car (car (s-matched-positions-all "\#\\+BEGIN_abstract" (buffer-string)))))
  (goto-char (+ (cdr (car (s-matched-positions-all "\#\\+END_abstract" (buffer-string)))) 1))
  (org-ascii-export-as-ascii)
  (switch-to-buffer "*Org ASCII Export*"))
