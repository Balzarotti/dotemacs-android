(defun nx/writegood-insert-template-at-point ()
  (interactive)
  (insert "# Writegood Scores:\n")
  (insert "# Reading Ease: 00.00\n")
  (insert "# Grade Level: 00.00\n"))


(defun nx/writegood-template-ease-exists-p ()
  "t if there is a line where to insert the reading ease"
  (s-matches-p "# [Rr]eading [Ee]ase:" (buffer-string)))

(defun nx/writegood-template-grade-exists-p ()
  "t if there is a line where to insert the grade level"
  (s-matches-p "# [Gg]rade [Ll]evel:" (buffer-string)))

(defun nx/writegood-insert-level ()
  (interactive)
  (message (replace-regexp-in-string (buffer-string) "\\(# [Gg]rade [Ll]evel: \\)[0-9]+\.[0-9]+" (format "\1: %s" 00.00)))
  )
