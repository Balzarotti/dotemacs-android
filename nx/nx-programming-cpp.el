(use-package cc-mode
  :defer t
  :init (subword-mode) ;; CamelCase mid-word
  (setq cc-search-directories
	'("."
	  "../src" "../include"
	  "../src/*" "../include/*"
	  "../../src" "../../include"
	  "../../src/*" "../../include/*"
	  "../../src/*/*" "../../include/*/*"))
  ;; hide long headers
  (add-hook 'c-mode-common-hook 'elide-head)
  :config
  (define-key c++-mode-map (kbd "C-c C-c") 'ff-find-other-file)
  (define-key c-mode-map (kbd "C-c C-c") 'ff-find-other-file))

;; Linters
(use-package flymake-google-cpplint :requires cc-mode
  :defer t :commands (flymake-google-cpplint-load)
  ;; FIXME: test/usage
  :config (add-hook 'c++-mode-hook 'flymake-google-cpplint-load))

(use-package irony :if (not is-win-p) :requires cc-mode
  :defer t
  :commands (irony-mode irony-version
			irony-cdb-autosetup-compile-options)
  :init
  (setq irony-server-install-prefix "~/.nix-profile/bin/irony-server")
  (add-hook 'c++-mode-hook 'irony-mode)
  (add-hook 'c-mode-hook 'irony-mode)
  (add-hook 'cc-mode-hook 'irony-mode)
  (add-hook 'objc-mode-hook 'irony-mode)
  (add-hook 'irony-mode-hook 'irony-cdb-autosetup-compile-options))

(use-package flycheck-irony :if (not is-win-p)
  :requires (irony cc-mode-hook)
  :defer t :commands (flycheck-irony-setup)
  :init
  (add-hook 'flycheck-mode-hook #'flycheck-irony-setup)
  (add-hook 'irony-mode-hook #'flycheck-mode))

(provide 'nx-programming-cpp)
