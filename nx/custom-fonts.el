(let ((font "Hack-9"))
  (add-to-list 'default-frame-alist '(font . "Hack-9"))
  (if (member (car (split-string font "-"))
	      (font-family-list))
      (progn
	(set-default-font font)
	(set-frame-font font))
    (message (format "Font %s not available! Install it and call set font to %s"
		     font font))))

(provide 'custom-fonts)
