(require 'nx-writing)
(require 'nx-papers)

(defvar org-blocks-hidden nil)
(defun org-toggle-blocks ()
  (interactive)
  (if org-blocks-hidden
      (org-show-block-all)
    (org-hide-block-all))
  (setq-local org-blocks-hidden (not org-blocks-hidden)))

;; org mode
;; (use-package typoel-mode)
(use-package org :defer t :after ess
  :init (setq org-use-speed-commands t
	      org-return-follows-link t
	      org-hide-emphasis-markers t
	      org-startup-truncated t
	      org-src-fontify-natively t
	      org-image-actual-width 550)
  :bind (("C-c c" . org-capture)
         :map org-mode-map
         ;; ("M-q" . org-fill-paragraph)
         ("M-q" . visual-line-mode)
	 ("C-c /" . org-mark-ring-goto)
	 ("C-c m" . nx/org-insert-macro)
	 ("C-c i" . nx/org-italics)
	 ("C-c u" . nx/org-underline)
	 ;; overrides: org-force-cycle-archived
	 ("C-<tab>" . smart-comment))
  :config
  (require 'org-protocol-cfg)
  (require 'nx-org-agenda)
  (require 'nx-ox-org)
  (require 'ox-ipynb)
  (require 'ox-taskjuggler) ;; requires org-plus-contrib
  (require 'org-sources)
  (require 'org-image)
  ;; Expand in ediff
  (add-hook 'ediff-prepare-buffer-hook #'show-all)
  ;; redisplay inline images after org-src evaluation
  (add-hook 'org-babel-after-execute-hook 'org-redisplay-inline-images)

  ;; inline images after block eval
  (add-hook 'org-mode-hook #'org-toggle-blocks)
  (add-hook 'org-mode-hook #'auto-fill-mode)
  (add-hook 'org-mode-hook #'flyspell-mode)
  (add-hook 'org-mode-hook #'abbrev-mode)

  (add-hook 'org-mode-hook #'flyspell-buffer)
  (add-hook 'org-mode-hook #'writegood-mode)

  ;; (add-hook 'org-mode-hook #'visual-line-mode)
  ;; tasks
  (setq org-log-done 'time)

  ;; fix ob-ipython https://github.com/gregsexton/ob-ipython/issues/158
  (defun ob-ipython--maybe-run-async ()
    (when (not (ob-ipython--running-p))
      (-when-let (val (ob-ipython--dequeue 'ob-ipython--async-queue))
	(cl-destructuring-bind (code name callback args) val
	  (ob-ipython--run-async code name callback args)))))
  ;; abo-abo complete-at-point
  ;; http://oremacs.com/2017/10/04/completion-at-point/
  (defun org-completion-symbols ()
    (when (looking-back "=[a-zA-Z]+")
      (let (cands)
	(save-match-data
	  (save-excursion
	    (goto-char (point-min))
	    (while (re-search-forward "=\\([a-zA-Z]+\\)=" nil t)
	      (cl-pushnew
	       (match-string-no-properties 0) cands :test 'equal))
	    cands))
	(when cands
	  (list (match-beginning 0) (match-end 0) cands)))))

  ;; easy markup
  (make-local-variable 'electric-pair-pairs)
  (setq-local electric-pair-pairs '(
				    (?\/ . ?\/);; italice
				    (?\_ . ?\_);; underscore
				    (?\~ . ?\~);; code
				    ;; used more for creating headings
				    ;; (?\* . ?\*);; bold
				    ;; used more freq. not for markup
				    ;; (?\= . ?\=);; verbatim
				    ;; (?\+ . ?\+);; strike
				    ))

  ;; if on a link, RET opens it, calling org-open-at-point
  (setq org-return-follows-link t)
  (setq org-highlight-latex-and-related '(latex)) ; latex $raw$ colorize
  (setq org-hide-macro-markers t);; #+MACRO: hides {{{}}} brackets
  (setq org-startup-indented t
	org-ellipsis " ▼"))

(use-package org-bullets :after org :defer t ;; Fancy utf-8 bullets for org mode
  :init (add-hook 'org-mode-hook (lambda () (org-bullets-mode 1)))
  :config
  (setq org-bullets-bullet-list '("⊢" "⋮" "◉" "○" "✸" "•" "•" "•")))

;; inline images after block eval

(use-package org-download :defer t :after org)

(provide 'nx-org)
