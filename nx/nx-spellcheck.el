;; spell checking + abbrev
(setq ispell-silently-savep t ;; save dictionary without asking
      save-abbrevs 'silently
      ;; faster on big buffers
      flyspell-issue-message-flag 'nil)

;; Must be configured to
;; - have spellchack in english by default
;; - toggle between the two with a keybind

;; - enable in programming language comments
(defun enable-flyspell-comments (language-mode-hook)
  (add-hook language-mode-hook
	    (lambda () (flyspell-prog-mode))))

;; TODO: CHECK ME
(enable-flyspell-comments 'c++-mode-hook)
(enable-flyspell-comments 'julia)
(enable-flyspell-comments 'python)

(let ((langs '("english" "italian")))
  (setq nx/flyspell-languages (make-ring (length langs)))
  (dolist (elem langs) (ring-insert nx/flyspell-languages elem)))

(defun cycle-ispell-languages ()
  (interactive)
  (let ((lang (ring-ref nx/flyspell-languages -1)))
    (ring-insert nx/flyspell-languages lang)
    (ispell-change-dictionary lang)
    (flyspell-buffer)))

;; Skip spelling in a region
(add-to-list 'ispell-skip-region-alist '("^#+BEGIN_SRC" . "^#+END_SRC"))

(global-set-key [f6] 'cycle-ispell-languages)

(define-key ctl-x-map "\C-i"
  #'endless/ispell-word-then-abbrev)

(provide 'nx-spellcheck)
