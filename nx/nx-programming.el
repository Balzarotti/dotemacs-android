;; Programming languages
(use-package ess :defer t
  :init
  (setq inferior-julia-program-name "julia")
  :config
  (require 'ess-site))

(use-package julia-repl :defer t :after term
  :commands (julia-repl)
  :mode ("\\.jl$" . julia-mode)
  :init (add-hook 'julia-mode-hook 'julia-repl-mode) ;; always use minor mode
  (add-hook 'julia-mode-hook #'glasses-mode);; camelCase and () space
  (add-hook 'julia-mode-hook #'c-subword-mode);; camelCase subword)
  (defalias 'julia 'julia-repl
	  "Easier to remember and overrides ess (julia) command")
  :config
  (use-package ob-julia))


;; FIXME: both on android and on nixos
(use-package meson-mode :defer t
  :mode "\\meson.build$")

;; Broken in nixos
;; (use-package cmake-mode :defer t
;;   :mode "^CMakeLists.cmake$")

(use-package lua-mode :defer t ; FIXME: on nixos
  :mode "\\.lua$")
(use-package arduino-mode :defer t ; FIXME: on nixos
  :mode "\\.pde$" "\\.ino$")
(use-package faust-mode :defer t
  :mode "\\.dsp$")
(use-package graphviz-dot-mode :defer t
  :mode "\\.dot$")
(use-package rust-mode :defer t
  :mode "\\.rs$")
(use-package cargo :defer t
  :commands (cargo-minor-mode)
  :mode "\\Cargo.toml$"
  :init (add-hook 'rust-mode-hook 'cargo-minor-mode))
(use-package octave :defer t
  :mode ("\\.m$" . octave-mode))

;; c/c++
(require 'nx-programming-web)
(require 'nx-programming-java)
(require 'nx-programming-cpp)

;; debuggers ;; TODO: try to use it and customize
;(use-package realgud ;; multiple languages
;  ;; https://github.com/realgud/realgud
					;  )

(provide 'nx-programming)
