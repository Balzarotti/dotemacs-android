(defun nx/capture-this-mail ()
  "Call this function on a notmuch-mail view to capture it to your inbox"
  (interactive)
  (let ((message-id (notmuch-show-get-message-id))
	(message-subject (notmuch-show-get-subject)))
    (org-capture "inbox" "t")
    (insert message-subject)
    (next-line)
    (insert (format "[[notmuch:%s][mail]]" message-id))))

(provide 'nx-org-capture-notmuch)
