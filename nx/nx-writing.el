;; writing tools
(use-package synosaurus ;; requires wn (wordnet)
  :defer t
  :config
  (defun synosaurus-lookup-word ()
    (interactive)
    (synosaurus-lookup (word-at-point))
    (switch-to-buffer-other-frame "*Synonyms List*"))
  (defun synosaurus-lookup-selection (start end)
    (interactive "r")
    (synosaurus-lookup (buffer-substring start end))
    (switch-to-buffer-other-frame "*Synonyms List*"))
  :bind
  ("C-c s l" . synosaurus-lookup)
  ("C-c s s" . synosaurus-lookup-selection)
  ("C-c s w" . synosaurus-lookup-word)
  ("C-c s r" . synosaurus-choose-and-replace))

(require 'nx-spellcheck)

(define-key ctl-x-map "\C-i"
  #'endless/ispell-word-then-abbrev)

(use-package wordnut :defer t
  :bind
  (;; Consistent with synosaurus
   ("C-c s d" . wordnut-lookup-current-word)
   ("C-c s D" . wordnut-search)
   :map wordnut-mode-map
   ;; q just kill the buffer insted of the window
   ("q" . spacemacs/kill-this-buffer)))

(use-package langtool :defer t :commands (langtool-check)
  :config
  ;; Use nix to install it!
  ;; langtool-language-tool-jar "~/.emacs.d/langtool/languagetool-commandline.jar"
  (setq langtool-mother-tongue "it"
	langtool-disabled-rules '("WHITESPACE_RULE"
				  ;; "EN_UNPAIRED_BRACKETS"
				  ;; "COMMA_PARENTHESIS_WHITESPACE"
				  "EN_QUOTES")))
(use-package writegood-mode :defer t
  :commands (writegood-mode
	     writegood-reading-ease writegood-grade-level))

;; like focuswriter
(use-package writeroom-mode :defer t :commands (writeroom-mode))

;; count lines, words ..
(use-package wc-mode :defer t :commands (wc wc-mode))
(use-package cloc :defer t :commands (cloc))

(use-package lorem-ipsum :defer t
  :commands (lorem-ipsum-insert-sentences
	     Lorem-ipsum-insert-paragraphs
	     lorem-ipsum-insert-list))

(provide 'nx-writing)
