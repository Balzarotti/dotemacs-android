(require 'nx-programming)

;; Other flle format
(use-package csv-mode ; FIXME: on nixos
  :mode "\\.csv$")
(use-package json-mode
  :mode "\\.json$")
(defun nx/yml-indent-list ()
  "ym: Newline, indent and insert a list item -"
  (interactive)
  (progn (newline-and-indent)
         (insert "- ")))

(use-package yaml-mode
  :mode "\\.yml$" "\\.yaml$"
  :config
  :bind (:map yaml-mode-map
              ("M-m" . nx/yml-indent-list)))
(use-package markdown-mode
  :mode "\\.md$")

(provide 'nx-languages)
