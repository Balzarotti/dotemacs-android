;; https://emacs.cafe/emacs/orgmode/gtd/2017/06/30/orgmode-gtd.html
(setq org-agenda-files '("~/org/inbox.org"    ;; collect everything
                         "~/org/gtd.org"      ;; projects
                         "~/org/tickler.org"));; timestamped reminders
(setq org-capture-templates ;; TODO: Customize
      '(("t" "Todo [inbox]" entry
         (file+headline "~/org/inbox.org" "Tasks")
         "* TODO %i %? [%]")
        ("T" "Todo (this file) [inbox]" entry
         (file+headline "~/org/inbox.org" "Tasks")
         "* TODO %? [%]\n  %i\n  %a")
        ;; ("n" "NixOS" entry (file "~/Scm/Git/org/nixos.org"))
        ;; ("j" "Journal" entry (file+deatetree "~/Scm/Git/org/journal.org")
        ;;  "* %?\nEntered on %U\n  %i\n  %a")
        ;; ("e" "emacs" entry (file "~/Scm/Git/org/emacs.org")))
        ("r" "Tickler (recurrent)" entry
         (file+headline "~/org/tickler.org" "Tickler")
         "* %i%? \n %U")
	("n" "nix (nix, NixOS, DevOps)" entry
         (file+headline "~/org/nix.org" "Useful Things")
         "* TODO %i %? [%]")))

(setq org-refile-targets '(("~/org/gtd.org" :maxlevel . 3)
                           ("~/org/someday.org" :level . 1)
                           ("~/org/tickler.org" :maxlevel . 2)))

(setq org-todo-keywords
      '((sequence "TODO(t)" "WAITING(w)" "|" "DONE(d)" "CANCELLED(c)")))

(provide 'nx-org-agenda)
