;; Close org-src with C-x C-s
(eval-after-load 'org-src
  '(define-key org-src-mode-map
     "\C-x\C-s" #'org-edit-src-exit))
;; Languages enabled

(org-babel-do-load-languages
 'org-babel-load-languages
 '(; Scripting
   (sh . t)
   (shell . t)
   (perl . t)
   (python . t)
   (ruby . t)

   ;; Math
   (julia . t)
   (ipython . t)
   (octave . t)
   (maxima . t)
   (R . t)

   ;; graphs and text
   (latex . t)
   (ditaa . t)
   (dot . t)

   ;; compiled
   (C . t)

   ;; others
   (sqlite . t)))

;; Allow those block to evaluate without confirmation
(defun ck/org-confirm-babel-evaluate (lang body)
  (not (or
        ;; Scripting
        (string= lang "sh")
	(string= lang "bash")
        ;; (string= lang "shell")
        (string= lang "emacs-lisp")
        (string= lang "perl")
        (string= lang "ruby")
        ;; Math
        (string= lang "octave")
        (string= lang "maxima")
        (string= lang "R")
        (string= lang "python")
        (string= lang "ipython")
        (string= lang "jupyter")
	(string= lang "jupyter-julia")
	(string= lang "jupyter-python")
	(string= lang "jupiter-R")
        (string= lang "julia")
        (string= lang "latex")
        (string= lang "dot")
	;; compiled
	(string= lang "cpp")
	(string= lang "C")
	)))

(setq org-confirm-babel-evaluate 'ck/org-confirm-babel-evaluate)

(setq org-src-window-setup 'current-window) ;; C-x n

(provide 'org-sources)
