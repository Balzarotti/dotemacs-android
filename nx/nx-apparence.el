(when gui-enabled
  (require 'custom-fonts)
  (tool-bar-mode -1)
  (scroll-bar-mode -1))
;; apparence
(require 'nx-theme)

;; Disable blink-cursor
;; it makes pdf flicker:
;; https://emacs.stackexchange.com/questions/13791/pdf-doc-view-flickers-continously#28599
(blink-cursor-mode 0)

(use-package smooth-scrolling :defer t
  :commands (smoot-scrolling-mode)
  :init
  (setq smooth-scroll-margin 4)
  (define-globalized-minor-mode
    my-global-smooth-scrolling-mode smooth-scrolling-mode
    (lambda () (smooth-scrolling-mode 1)))
  (my-global-smooth-scrolling-mode 1))

;; (use-package perspective
;;   :config
;;   (persp-mode))

(use-package powerline
  :config
  (setq powerline-display-buffer-size nil)
  (setq powerline-display-mule-info nil)
  (powerline-default-theme))

(use-package column-enforce-mode :defer 1
  ;; no more than 80 chars lines
  :config (global-column-enforce-mode t))

(use-package linum-relative :defer t
  :commands (linum-relative-mode
	     linum-relative-global-mode
	     linum-relative-toggle)
  :config
  (setq linum-relative-current-symbol "") ;; show current line
  ;; enable with (linum-relative-global-mode)
  )

(use-package glasses :defer t
  :commands (glasses-mode))

(use-package rainbow-identifiers :defer t
  :commands (rainbow-identifiers-mode))

(use-package rainbow-mode :defer t ;; hex color codes
  :commands (rainbow-mode))

;; better than highlight-symbol + works
(use-package highlight-thing :defer 10
  ;; don't use the global if you open big files (like 110K csv).
  :init (add-hook 'prog-mode-hook 'highlight-thing-mode)
  :config
  (setq highlight-thing-case-sensitive-p t
	highlight-thing-limit-to-defun 'nil
	highlight-thing-exclude-thing-under-point t
	highlight-thing-prefer-active-region t
	highlight-thing-ignore-list '("False" "True" "false" "true" "t" "nil")
	)
  :commands (global-highlight-thing-mode
	     highlight-thing-mode))

(use-package hl-todo :defer 8
  :init (global-hl-todo-mode))

(provide 'nx-apparence)
