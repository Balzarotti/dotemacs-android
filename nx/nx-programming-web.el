;; Web development
(use-package emmet-mode
  :config
  ;; Auto-start on any markup modes
  (add-hook 'sgml-mode-hook 'emmet-mode)
  ;; enable Emmet's css abbreviation.
  (add-hook 'css-mode-hook 'emmet-mode)
  (add-hook 'web-mode 'emmet-mode)
  :bind (:map emmet-mode-keymap
	      ("<tab>" . emmet-expand-line)))

(use-package web-mode
  :mode "\\.phtml$"
  "\\.html$"
  "\\.hbs$"
  "\\.tpl$"
  "\\.php$"
  "\\.[agj]sp$"
  "\\.as[cp]x$"
  "\\.erb$"
  "\\.mustache$"
  "\\.djhtml$")

;; FIXME: wait until
;; https://github.com/segv/jss
(use-package jss
  :mode "\\.js$"
  )

;; reactjs
;; (use-package jsx-mode :after web-mode
;;   (defun auto-load-react-enable ()
;;     (interactive)
;;     (setq web-mode-content-types-alist
;; 	  '(("jsx"  . "\\.js[x]?\\'")))))
;; ember
;; (use-package ember-mode :after web-mode
;;   :diminish (ember-mode . "emb")
;;   :init
;;   ;; If you are going to edit ember files
;;   ;; call the following function
;;   (defun auto-load-ember-enable ()
;;     (interactive)
;;     (add-hook 'web-mode-hook 'ember-mode)
;;     (add-hook 'js-mode-hook 'ember-mode)
;;     (add-hook 'js2-mode-hook 'ember-mode)))

;; (use-package skewer-mode)
;; (use-package impatient-mode)
;; (use-package react-snippets)
;; (use-package php-mode)
;; (use-package palette) ;; FIXME: on nixos

;; ;; create new RoR app
;; (use-package rails-new)

(provide 'nx-programming-web)
