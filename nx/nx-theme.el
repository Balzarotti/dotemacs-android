(use-package solarized-theme :disabled ;; WIP
  :init
  ;; Avoid all font-size changes
  (setq solarized-height-minus-1    1.0
	solarized-height-plus-1     1.03
	solarized-height-plus-2     1.06
	solarized-height-plus-3     1.09
	solarized-height-plus-4     1.12
	x-underline-at-descent-line t)
  :config
  ;; (message "Not loading the theme"))
  )

(use-package leuven-theme ;; white theme for org mode
  :defer t
  ;; :config
  ;; (setq leuven-scale-outline-headlines nil)
  ;; (setq leuven-scale-org-agenda-structure nil)
  )

;; Requires mplayer (play music)
(use-package nyan-mode :defer t
  :commands (nyan-mode))

(defface nx/org-highlight '((t (:background  "yellow" :foreground  "black"
					     :invisible t)))
  "nx/org-highlight")

;; set cursor color
(add-to-list 'default-frame-alist '(mouse-color . "gray"))
(if gui-enabled
    (set-mouse-color "gray"))


;; (if (display-graphic-p)
;;     (load-theme 'solarized-dark)
;;   (load-theme 'leuven))

;; FIXME: Have a variable to define the themes

;; Different themes on terminal / GTK
;; https://lists.gnu.org/archive/html/help-gnu-emacs/2012-02/msg00237.html

(defun mb/pick-color-theme (frame)
  (select-frame frame)
  (if (window-system frame)
      (progn
	(disable-theme 'leuven) ; in case it was active
	(enable-theme 'solarized-dark))
    (progn
      (disable-theme 'solarized-dark) ; in case it was active
      (enable-theme 'leuven))))
(add-hook 'after-make-frame-functions 'mb/pick-color-theme)

;; last t to not enable it
(load-theme 'solarized-dark t t)
(load-theme 'leuven t t)

;; For when started with emacs or emacs -nw rather than emacs --daemon
(if window-system
    (enable-theme 'solarized-dark)
  (enable-theme 'leuven))

(provide 'nx-theme)
