;; Git, backup, history
(use-package magit
  :bind
  ("C-x g" . magit-status)
  :init
  (setq magit-completing-read-function 'ivy-completing-read)
  (magit-wip-after-save-mode))
;; show git file changes in sidebar
(use-package git-gutter
  :defer 1
  :diminish (git-gutter-mode . "g+")
  :config
  (git-gutter)
  ;;(git-gutter:linum-setup) ;; fix linum
  (global-git-gutter-mode +1))


(use-package git-wip-timemachine :defer t
  :commands (git-wip-timemachine
	     git-wip-timemachine-mode
	     git-wip-timemachine-toggle))

(provide 'nx-git)
