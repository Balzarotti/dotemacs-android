;; A bit of configuration here

(defvar default-bibliography-file "~/bibliography/references.bib"
  "File to use as the default bib file in all the packages that
requires it")
;; TODO: check org-bibtex (bib in org mode)

;; Show/annotate pdfs
(use-package pdf-tools :defer t :if is-nix-p
  :mode "\\.pdf'"
  :config
  (pdf-tools-install)
  (add-to-list 'org-file-apps '("\\.pdf\\'" . (lambda (file link) (org-pdfview-open link))))
  ;; remove (trim) white margin for more viewing space
  (add-hook 'pdf-tools-modes 'pdf-view-auto-slice-minor-mode)
  (setq pdf-view-resize-factor 1.1
	pdf-view-display-size 'fit-page)
  :bind (:map pdf-view-mode-map
              ("\\" . hydra-pdftools/body)
              ("C-s" . pdf-occur) ;; override counsel
              ("<M-spc>" .  pdf-view-scroll-down-or-next-page)
              ("g"  . pdf-view-first-page)
              ("G"  . pdf-view-last-page)
              ("l"  . image-forward-hscroll)
              ("h"  . image-backward-hscroll)
              ("j"  . pdf-view-next-page)
              ("k"  . pdf-view-previous-page)
              ("e"  . pdf-view-goto-page)
              ("u"  . pdf-view-revert-buffer)
              ("al" . pdf-annot-list-annotations)
              ("ad" . pdf-annot-delete)
              ("aa" . pdf-annot-attachment-dired)
              ("am" . pdf-annot-add-markup-annotation)
              ("ah" . pdf-annot-add-highlight-markup-annotation)
              ("at" . pdf-annot-add-text-annotation)
              ("y"  . pdf-view-kill-ring-save)
              ("I"  . pdf-misc-display-metadata)
	      ("i"  . interleave-open-notes-file-for-pdf)
              ("s"  . pdf-occur)
              ("/"  . isearch-forward)
              ("b"  . pdf-view-set-slice-from-bounding-box)
              ("r"  . pdf-view-reset-slice)
              ("D"  . pdf-view-midnight-minor-mode)))

;; Annotate pdfs
(use-package interleave :defer t
  :commands (interleave interleave-open-notes-file-for-pdf)
  :if gui-enabled
  ;; ADD to the org file:
  ;; #+INTERLEAVE_PDF: /the/path/to/pdf.pdf
  )


;; Search paper on gscholar, ACM, IEEE, DBLP
(use-package gscholar-bibtex
  :defer t :commands (gscholar-bibtex)
  :config
  (setq gscholar-bibtex-database-file default-bibliography-file))

;; FIXME: Pub-mode is not on melpa but worked quite well
;; Manage the bibliography
(use-package ivy-bibtex :requires ivy
  :after org :defer t :commands (ivy-bibtex)
  :init	; You can add other bib files
  ;; TODO: supports org-bibtex too
  (setq bibtex-completion-bibliography default-bibliography-file
	bibtex-completion-library-path "~/Dropbox/bibliography/bibtex-pdfs"
	bibtex-completion-notes-path "~/bibliography/notes.org"
        ;; bibtex-completion-pdf-field "file" ;; FIXME: breaking a lot of things
        ))

(use-package org-ref :after org :defer t
  :commands (org-ref-ivy-cite org-ref-ivy-cite-completion org-ref)
  :config
  (require 'doi-utils)
  (require 'org-ref-bibtex)
  (require 'org-ref-ivy-cite)
  ;; use uuid as link inside org documents
  (require 'org-id)
  (setq org-ref-completion-library 'org-ref-ivy-cite
	reftex-default-bibliography '("~/bibliography/references.bib")
	org-ref-bibliography-notes "~/bibliography/notes.org"
	org-ref-default-bibliography default-bibliography-file
	org-ref-pdf-directory "~/bibliography/bibtex-pdfs"))


(use-package org-pdfview :defer t :commands (org-pdfview-open))

(use-package magic-latex-buffer
  :defer t :commands (magic-latex-buffer)
  :init (add-hook 'latex-mode-hook 'magic-latex-buffer))

(use-package slirm ;; Systematic literature review on bibtex
  ;; call with slirm-start
  :defer t :commands (slirm-start))

(use-package org-mind-map :disabled
  ;; to export call org-mind-map-write
  ;; See the github readme for examples
  ;; https://github.com/theodorewiles/org-mind-map
  )

(use-package tex :ensure auctex)

(provide 'nx-papers)
