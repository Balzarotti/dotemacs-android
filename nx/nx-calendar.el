;; Calendar
(setq calendar-week-start-day 1 ;; Start on Monday
      sentence-end-double-space nil)

(provide 'nx-calendar)
